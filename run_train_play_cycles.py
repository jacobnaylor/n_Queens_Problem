import numpy as np, pandas as pd, random, sys, copy
from train_neural_net import *
from keras.models import load_model


def predict_board_value(pred_board,regressor):

    _, features = pred_board.get_features() 
    num_pieces = pred_board.count_pieces()
    
    y = [num_pieces]+features
    y = np.array(y).reshape(1,-1)
    return regressor.predict(y)

    
class Board:
    def __init__(self,s=4):
        self.size = s
        self.matrix = np.zeros(shape=(s,s),dtype=np.int8)
        self.reset()
        
    def reset(self):
        self.matrix = np.zeros(shape=(self.size,self.size),dtype=np.int8)
        
    def increment_square(self,coords=(0,0)):
        assert len(coords) == 2, "coords should be 2 dimensions only"
        assert coords[0] <= self.size and coords[1] <= self.size, "coords out of bounds"
        self.matrix[coords[0],coords[1]] = self.matrix[coords[0],coords[1]] + 1
        
    def check_board_fail(self):
        # TODO cache these sums to avoid recalculating them
        row_sums,col_sums,left_sums,right_sums = self.get_sums()[1]

        for sum_type in [row_sums,col_sums,left_sums,right_sums]:
            if max(sum_type) > 1:
                # Board Failed
                return True
            else:
                pass
        return False
           
    def get_sums(self):
        row_sums = self.row_sums()
        col_sums = self.col_sums()
        left_sums, right_sums = self.diag_sums()
        
        # generate headers
        row_headers = []
        col_headers = []
        left_headers = []
        right_headers = []
        
        for i in range(len(row_sums)):
            row_headers.append("row"+str(i))
            
        for i in range(len(col_sums)):
            col_headers.append("col"+str(i))
            
        for i in range(len(left_sums)):
            left_headers.append("left"+str(i))

        for i in range(len(right_sums)):
            right_headers.append("right"+str(i))
        
        return [(row_headers, col_headers, left_headers, right_headers), (row_sums, col_sums, left_sums, right_sums)]
        
    def row_sums(self):
        sums = []
        for x in range(self.size):
            sums.append(self.matrix[x, :].sum())
        return sums
    
    def col_sums(self):
        sums = []
        for y in range(self.size):
            sums.append(self.matrix[:, y].sum())
        return sums
    
    def diag_sums(self):
        left = self.left_diag_sums()
        right = self.right_diag_sums()
        return (left, right)
        
    def left_diag_sums(self):
        sums = []
        for i in range(-(self.size-1), (self.size)):
            sums.append(np.trace(self.matrix,i))
        return sums
    
    def right_diag_sums(self):
        sums = []
        temp = np.fliplr(self.matrix)
        for i in range(-(self.size-1), (self.size)):
            sums.append(np.trace(temp,i))
        return sums
    
    def count_pieces(self):
        return self.matrix.sum()
    
    def get_features(self):
        # Calculate Features
        headers, features = self.get_sums() # TODO Add ability to return column names from get_sums method
        features = [item for sublist in features for item in sublist]  # Flatten Nested Lists
        features.append(max(features))

        headers = [item for sublist in headers for item in sublist]
        headers.append("max_sum")
        return headers, features
    

def play_game(regressor=None, rand_search=False):
    # Generate Training Data
    # Open training data log
    out_path = "/Users/jacob.naylor/Code/n_Queens_Problem/data/training_data.txt"
    write_mode = 'a'
    write_header = False
    search_volume = 0.5  # Need to Scale Search Volume as # Pieces on Board Increases ?

    # Start new Board
    n=9
    board = Board(n)
    #training_data = pd.DataFrame()
    training_data = []
    for iteration in xrange(50000):

        headers, features = board.get_features()

        # Check Board Status
        num_pieces = board.count_pieces()
        board_fail = board.check_board_fail()

        training_data.append([num_pieces]+features)
        headers = ["num_pieces"] + headers

        if board_fail:
            board.reset()
            df = pd.DataFrame(training_data,columns=headers)
            df["final_score"] = num_pieces-1
            df.to_csv(out_path, index=False, header=write_header, mode=write_mode) #This will probably be extremely slow.
            write_mode='a'
            write_header=False
            training_data = []
        elif num_pieces == n:
            print "SUCCESS!!!!!"
            print board.matrix
            return "Success",board.matrix
            break
        else:
            # Search for Coord to Increment
            if rand_search:
                x = random.randrange(0,n)
                y = random.randrange(0,n)
                coord = (x,y)
                board.increment_square(coord)
            else:
                search_results = {}
                for i in range(max(int((n^2)*search_volume), 2)):
                    x = random.randrange(0, n)
                    y = random.randrange(0, n)
                    candidate_coord = (x, y)
                    eval_board = copy.deepcopy(board)
                    eval_board.increment_square(candidate_coord)
                    pred_val = predict_board_value(eval_board,regressor)
                    search_results[candidate_coord] = pred_val

                # Extract Best Coord from Search
                coord = max(search_results, key=search_results.get)
                # Increment, n+=1
                board.increment_square(coord)
    return None,None


if __name__ == '__main__':

    test = False
    rand_search = False
    num_epochs = 1
    kwargs = {"test":test}
    
    if rand_search == False:
        print "Training from Scratch"
        regressor = train_model(**kwargs)
    else:
        print "Running Random Search, Skipping Training"
        regressor = None
        
    print "Playing Game"
    result = play_game(regressor,rand_search)
    if result[0] == "Success":
        print result[0]
        print result[1]
    """
    try:
        print "Attempting to Load Existing Model"
        regressor = load_model('neural_net_model.h5')
    except:
        print "Existing Model Load Failed - Training from Scratch"
        regressor = train_model(**kwargs)
    
    
    for epoch in range(num_epochs):
        print "Starting Epoch: ", epoch
        print "Playing Game"
        result = play_game(regressor,rand_search)
        if result[0] == "Success":
            print result[0]
            print result[1]
            break
            
        print "Retraining Regressor"
        regressor = retrain_model(regressor,**kwargs)
        regressor.save('neural_net_model.h5')
    """