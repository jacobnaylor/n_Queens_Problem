import numpy as np, pandas as pd, random, sys, copy

from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import GridSearchCV


def pull_training_data(training_data_path="/Users/jacob.naylor/Code/n_Queens_Problem/data/training_data.txt",test=False):
    
    if test:
        num_rows = 10000
    else:
        num_rows = None
    
    # Pull data
    training_data = pd.read_csv(training_data_path, header=0, nrows=num_rows)
    y = training_data["final_score"]
    training_data.drop("final_score", axis=1, inplace=True)
    X = training_data.values
    return X, y


# Train a Neural Network Model
def baseline_model():
    # create model
    model = Sequential()
    model.add(Dense(60, input_dim=54, kernel_initializer='normal', activation='relu'))
    model.add(Dense(30, kernel_initializer='normal', activation='relu'))
    model.add(Dense(15, kernel_initializer='normal', activation='relu'))
    model.add(Dense(7, kernel_initializer='normal', activation='relu'))
    model.add(Dense(1, kernel_initializer='normal'))
    # Compile model
    model.compile(loss='mean_squared_error', optimizer='adam')
    return model


def train_model(test=False):
    # fix random seed for reproducibility
    seed = 7
    np.random.seed(seed)
    estimator = KerasRegressor(build_fn=baseline_model, verbose=0)

    X, y = pull_training_data(test=test)

    parameters = {"nb_epoch": [125, 150, 175], "batch_size": [32, 64, 128]}

    regressor = GridSearchCV(estimator, parameters,n_jobs=5)
    regressor.fit(X,y)

    return regressor


def retrain_model(regressor,test=False):
    X, y = pull_training_data(test=test)
    regressor.fit(X, y)
    return regressor
